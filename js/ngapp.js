"use strict";

var webApp = angular.module("webApp", ["ngSanitize"]);
webApp.controller("AppController", function($scope){

  var poemStorage = {
    dark: {
      stanza: 1,
      lines: 3,
      words: 4,
      text: "Once upon a midnight dreary, while I pondered weak and weary, \
Over many a quaint and curious volume of forgotten lore, \
While I nodded, nearly napping, suddenly there came a tapping, \
As of some one gently rapping, rapping at my chamber door. \
'Tis some visitor, I muttered, `tapping at my chamber door \
Only this, and nothing more \
Ah, distinctly I remember it was in the bleak December, \
And each separate dying ember wrought its ghost upon the floor. \
Eagerly I wished the morrow; - vainly I had sought to borrow \
From my books surcease of sorrow - sorrow for the lost Lenore \
For the rare and radiant maiden whom the angels named Lenore \
Nameless here for evermore"
    },

    love: {
      stanza: 2,
      lines: 3,
      words: 4,
      text: "Kiss of my love \
Your beauty overwhelms me \
As I wrap my arms around you \
I press your softness tight \
Great passion fills my inner being \
I'm captured in your embrace \
Your eyes control my very soul \
The touch of your lips, heaven \
Forever frozen in time \
All else fades into nothing \
I love thee, as I love the calm \
    Of sweet, star-lighted hours! \
I love thee, as I love the balm \
    Of early jes'mine flow'rs. \
I love thee, as I love the last \
    Rich smile of fading day, \
Which lingereth, like the look we cast, \
    On rapture pass'd away. \
I love thee as I love the tone \
    Of some soft-breathing flute \
Whose soul is wak'd for me alone, \
    When all beside is mute"
    },
    fantasy: {
      stanza: 3,
      lines: 4,
      words: 4,
      text: "My first thought was, he lied in every word, \
That hoary cripple, with malicious eye \
Askance to watch the working of his lie \
On mine, and mouth scarce able to afford \
Suppression of the glee that pursed and scored \
Its edge, at one more victim gained thereby \
For, what with my whole world-wide wandering, \
What with my search drawn out thro' years, my hope \
Dwindled into a ghost not fit to cope \
With that obstreperous joy success would bring, \
I hardly tried now to rebuke the spring \
My heart made, finding failure in its scope \
There they stood, ranged along the hillsides, met \
To view the last of me, a living frame \
For one more picture! in a sheet of flame \
I saw them and I knew them all. And yet \
Dauntless the slug-horn to my lips I set, \
And blew to the Dark Tower came."
    },

    haiku: {
      stanza: 1,
      lines: 1,
      words: 7,
      text: "I feel that I've come - a long way for somebody - born with no clothes on \
Matter from lost socks - comes back to our universe - as wire coat hangers \
Row behind first class - Eat nuts and glimpse nirvana - Beyond the curtain \
Daydream believer Granite skies have pressed - fields to diamond. Our voices - fracture brittle air \
Pancakes, waffles, toast, - styrofoam. Mere vehicles - for maple syrup \
I see no icebergs! - Just keep it full steam ahead \
Man drowns in muesli - after having been pulled in - by a strong currant \
at the age old pond a frog leaps into water a deep resonance"

    }
  };

  $scope.poem = "";
  $scope.poemSource = null;
  $scope.displayCustom = false;

  $scope.customPoem = function () {
    if ($scope.poemSource == null) {
      $scope.poemSource = {
        stanza: 1,
        lines: 1,
        words: 4,
        text: "put some words here"
      }
    }
  }

  $scope.setPoemSource = function(poemType) {
    return $scope.poemSource = {
      stanza: poemStorage[poemType].stanza,
      lines: poemStorage[poemType].lines,
      words: poemStorage[poemType].words,
      text: poemStorage[poemType].text
    };
  }

  /**
   * generates THE POEM, expects a poemSourceObj
   */
  $scope.generatePoem = function (poemSourceObj) {
    // if poemSourceObj is a string we need to search for it in the poemStorage
    if (typeof(poemSourceObj) == "string") {
      poemSourceObj = $scope.setPoemSource(poemSourceObj);
    }

    var dictionary = [];
    var out_html = "";
    $.each( poemSourceObj.text.split("\n"), function( idxl, line){
      $.each( line.split(" ") , function(idxw, word ){
      var new_word = word.toLowerCase();
      if ( new_word == "i" ) { new_word = "I"; }
      if ( new_word.length > 0 && dictionary.indexOf( new_word ) == -1 ) {
        dictionary.push( new_word );
      }
      });
    });

    if ( dictionary.length > 0 ) {
      for( var i=0; i<poemSourceObj.stanza; i++ ) {
        out_html +="<p>"
        for ( var j=0; j<poemSourceObj.lines; j++ ) {
          out_html += "<section class=\"line\">";
          for( var k=0; k<poemSourceObj.words; k++ ) {
            out_html += dictionary[Math.floor(Math.random()*dictionary.length)] + " ";
          }
          out_html += "</section><br />";
        }
        out_html += "</p><p></p>";
      }
    }
    $scope.poem = out_html;
  }
});
